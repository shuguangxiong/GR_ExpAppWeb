var app = angular.module('starter.controllers', []);

app.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  var storage = window.localStorage;
  var Courier = AV.Object.extend('Courier');
  var Config = AV.Object.extend('Config');
  //console.log(storage.getItem('name'));
  //if(storage.getItem('uid')==null){
  //  $ionicModal.fromTemplateUrl('templates/login.html').then(function(modal) {
  //      modal.show();
  //  });
  //}
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  // Form data for the login modal
  $scope.loginData = {username:"",password:"",namenull:true,pwdnull:true};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });
  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    //console.log('Doing login', $scope.loginData);
    //console.log(typeof ($scope.loginData));
    if($scope.loginData.username==""){
      async.auto({
        func1:function(callback,results){
          console.log(1);
          callback(null,1);
        },
        func2:function(callback,results){
          console.log(2);
          callback(null,2);

        },
        func3:['func1',function(callback,results){
          console.log(3);
          callback(null,3);

        }],
        func4:['func2','func3',function(callback,results){
          console.log(results);
          callback(null,4);

        }]
      })
      $scope.loginData.namenull=false;
    }else if($scope.loginData.password==""){
      $scope.loginData.namenull=true;
      $scope.loginData.pwdnull=false;
    }else{
      $scope.loginData.pwdnull=true;

      var query = new AV.Query(Courier);
      var queryConfig=new AV.Query(Config);
      query.equalTo("name",$scope.loginData.username)
      query.equalTo("pwd",$scope.loginData.password)
      query.first().then(function(res){
        //console.log(res.id);
        if(res){
          var user={
            uid:res.id,
            name:res.get("name"),
            jobnumber:res.get("jobnumber"),
            areaid:res.get("area"),
            phone:res.get("phone")
          }
          var userJson=JSON.stringify(user);
          storage.setItem("uid",res.id);
          storage.setItem("user",userJson);
          $scope.modal.hide();
        }else{
          alert("login fail!");
        }
      })
    }
  };
})

.controller('PlaylistsCtrl', function($scope) {
      $scope.playlists = [
        { title: 'Reggae', id: 1 },
        { title: 'Chill', id: 2 },
        { title: 'Dubstep', id: 3 },
        { title: 'Indie', id: 4 },
        { title: 'Rap', id: 5 },
        { title: 'Cowbell', id: 6 }
      ];
})


