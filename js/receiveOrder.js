app.controller('receiveOrderCtrl', function($scope, $stateParams,$ionicModal, $timeout) {

     $scope.getOrdersArray = function(){

         var storage = window.localStorage;
         var uid = storage.getItem("uid");
         alert('uid:'+uid);

         var Order = AV.Object.extend("Order");
         var orderQuery = new AV.Query(Order);

         var Courier = AV.Object.extend("Courier");
         var courierQuery = new AV.Query(Courier);

         var Config  =AV.Object.extend("Config");
         var configQuery = new AV.Query(Config);

         courierQuery.get(uid).then(function(res){
             $scope.user = res;
         }).catch(function(err){
             console.log(err);
         });

         configQuery.equalTo('key','area');
         configQuery.find().then(function(res){
             //$scope.area = res.get('info').$scope.user.get('area');
         }).catch(function(err){
             console.log(err);
         });

         orderQuery.equalTo("status",6000);
         //orderQuery.equalTo("areaId",$scope.user.get('area'));
         orderQuery.find().then(function(res){
             $scope.ordersArray = res;
         }).catch(function(err){
             console.log(err);
         });
     }

     $scope.getOrdersArray();
});
